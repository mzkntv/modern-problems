#include "logger.h"
#include <fstream>
#include <string>
#include <iostream>

static const char logFileName[] = "log.txt";
Logger::Logger() {
    logFile.open(logFileName);
}

Logger::~Logger() {
    logFile << "Closing file" << std::endl;
    logFile.close();
}

void Logger::writeToLogFile(const std::string &msg) {
    logFile << msg << std::endl;
}
