#pragma once
#ifndef _SINGLETON_H_
#define _SINGLETON_H_
template <class T>
class Singleton
{
public:
    static T& instance() {
        if (myInstance == 0) {
            myInstance = new T;
        }
        return *myInstance;
    }
    static void terminate() {
        if (myInstance != 0) {
            delete myInstance;
            myInstance = 0;
        }
    }
protected:
    static T* myInstance;
    Singleton() {}
    Singleton(Singleton const &) {}
    Singleton& operator=(Singleton const &) {
        return *this;
    }
};

template <class T>
T* Singleton<T>::myInstance = 0;

#endif
