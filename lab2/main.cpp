#include <iostream>
#include "logger.h"

int main() {
    Logger::instance().writeToLogFile("Message 1");
    Logger::instance().writeToLogFile("Message 2");

    // Ensure that Logger instance is singleton - memory address is equal for each call of instance()
    std::cout << &Logger::instance() << std::endl;
    std::cout << &Logger::instance() << std::endl;

    // Logger::terminate();
    static LoggerWithDestruction l;

    return 0;
}