#pragma once
#ifndef _LOGGER_H_
#define _LOGGER_H_
#include "singleton.h"
#include <fstream>
#include <string>

class Logger : public Singleton<Logger> {
    std::ofstream logFile;
public:
    Logger();
    ~Logger();
    void writeToLogFile(const std::string &msg);
    friend class Singleton<Logger>;
};

class LoggerWithDestruction : public Singleton<LoggerWithDestruction> {
public:
    ~LoggerWithDestruction() {
        terminate();
    }
};

#endif
