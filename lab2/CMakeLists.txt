cmake_minimum_required(VERSION 3.26)	# Проверка версии CMake.
                                        # Если версия установленой программы
                                        # старее указаной, произайдёт аварийный выход.

project(hello_world)			        # Название проекта

set(SOURCE_EXE main.cpp)		        # Установка переменной со списком исходников для исполняемого файла
set(SOURCE_LIB logger.cpp)			    # Тоже самое, но для библиотеки
set (CMAKE_CXX_STANDARD 11)             # Use C++11 standard

add_library(logger STATIC ${SOURCE_LIB})	# Создание статической библиотеки с именем logger

add_executable(main ${SOURCE_EXE})	    # Создает исполняемый файл с именем main

target_link_libraries(main logger)		# Линковка программы с библиотекой
