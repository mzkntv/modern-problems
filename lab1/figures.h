#ifndef __FIGURES_H
#define __FIGURES_H
#include <vector>

class Figure {
    int color;
public:
    virtual int getColor() const {
        return color;
    }
    Figure(int color) : color(color) {}
    virtual ~Figure() {};
    
    friend std::ostream& operator<< (std::ostream &, const Figure &);
    
    virtual void print(std::ostream &) const = 0;
    virtual double place() const = 0;
    virtual double perimeter() const = 0;
    // virtual bool intersectTo(const Figure&) = 0;
    struct Point {
        int x, y;
        Point(int x, int y): x(x), y(y) {}
    };
};

class Circle : public Figure {
    Point center;
    int rad;
public:
    Circle(Point center, int rad, int color) : Figure(color), center(center), rad(rad) {}
    virtual ~Circle() {};
    virtual double place() const;
    virtual double perimeter() const;
    virtual void print(std::ostream &) const;
};

class Triangle : public Figure {
    Point p1, p2, p3;
public:
    Triangle(Point p1, Point p2, Point p3, int color) : Figure(color), p1(p1), p2(p2), p3(p3) {}
    virtual ~Triangle() {};
    virtual double place() const;
    virtual double perimeter() const;
    virtual void print(std::ostream &) const;
};

class Polygon : public Figure {
private:
    std::vector<Point>& points;
public:
    Polygon(std::vector<Point>&, int);
    virtual ~Polygon() {};
    virtual double place() const;
    virtual double perimeter() const;
    virtual void print(std::ostream &) const;
};
#endif
