#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <vector>
#include <ctime>
#include "figures.h"


int main() {
    srand((unsigned int)time(NULL));
    const int nFigures = 2;
    std::vector<Figure *> figures (nFigures);
    std::vector<Figure::Point> points {
        Figure::Point(1, 1), 
        Figure::Point(1, 3),
        Figure::Point(2, 1)
    };
    
    for (int i = 0; i < nFigures; i++) {
        if (i % 2 == 0) {
            figures[i] = new Polygon(
                points,
                rand() % 100
            );
        } else {
            figures[i] = new Triangle(
                points[0],
                points[1],
                points[2],
                rand() % 100
            );
        }
    }

    double sumPlace = 0.0;
    double sumPerimeter = 0.0;
    for (auto & figure : figures) {
        std::cout << *figure << std::endl;
        sumPlace += figure->place();
        sumPerimeter += figure->perimeter();
    }
    std::cout << std::fixed << std::setprecision(6) << "Sum place = " << sumPlace << std::endl;
    std::cout << std::fixed << std::setprecision(6) << "Sum perimeter = " << sumPerimeter << std::endl;
    for (int i = 0; i < nFigures; i++) {
        delete figures[i];
    }
    
    return 0;
}
