#define _USE_MATH_DEFINES
#define MAX_POINT_COUNT 10
#include <math.h>
#include <cmath>
#include "figures.h"
#include <iostream>
#include <vector>


std::ostream& operator<< (std::ostream &out, const Figure &figure) {
    figure.print(out);

    return out;
}

void Circle::print(std::ostream &out) const {
    out << "<Circle: x=" << center.x << ", y=" << center.y << ", r=" << rad << ", color=" << getColor() << ">";
}

void Triangle::print(std::ostream &out) const {
    out << "<Triangle: p1=(" << p1.x << ", " << p1.y << "), p2=(" << p2.x << ", " << p2.y << "), p3=(" << p3.x << ", " << p3.y <<  "), color=" << getColor() << ">";
}

void Polygon::print(std::ostream &out) const {
    out << "<Polygon: ";

    for (unsigned int i = 0; i < points.size(); i++ ) {
        out << "p" << i << "=(" << points[i].x << ", " << points[i].y << "), ";
    }
    out << "color=" << getColor() << ">";
}

double Circle::place() const {
    return M_PI * rad * rad;
}

double Circle::perimeter() const {
    return 2 * M_PI * rad;
}

double Triangle::place() const {
    double ans = 0.5 * ((p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y));
    if (ans < 0) ans = -ans;
    return ans;
}

double Triangle::perimeter() const {
    double ab = sqrt(fabs(pow(p2.x - p1.x, 2) + pow(p2.y - p1.y, 2)));
    double bc = sqrt(fabs(pow(p3.x - p2.x, 2) + pow(p3.y - p2.y, 2)));
    double ac = sqrt(fabs(pow(p3.x - p1.x, 2) + pow(p3.y - p1.y, 2)));

    return ab + bc + ac;
}

Polygon::Polygon(std::vector<Point>& points, int color) : Figure(color), points(points) {}

double Polygon::place() const {
    double ans = 0;
    
    for (unsigned i = 0; i < points.size(); i++) {
        Point p1 = i ? points[i - 1] : points.back();
        Point p2 = points[i];

        ans += (p1.x - p2.x) * (p1.y + p2.y);
    }

    return fabs(ans) / 2;
}

double Polygon::perimeter() const {
    double ans = 0;

    for (unsigned i = 0; i < points.size(); i++) {
        Point p1 = i ? points[i - 1] : points.back();
        Point p2 = points[i];

        ans += sqrt(fabs(pow(p2.x - p1.x, 2) + pow(p2.y - p1.y, 2)));
    }
    return ans;
}
